package com.greatfanchao.tmi.model

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

/**
 * Full category dto
 */
data class Category @JsonCreator constructor(
    @JsonProperty("Number") val id: String,
    @JsonProperty("Name") val name: String,
    @JsonProperty("Path") val path: String?,
    @JsonProperty("Subcategories") val subcategories: List<Category>?,
    @JsonProperty("Count") val count: Int,
    @JsonProperty("IsLeaf") val leaf: Boolean
) {
    fun toDescription(): CategoryDescription {
        return CategoryDescription(id = id, name = name)
    }
}

/**
 * A compact, minimal representation of a category
 */
@Parcelize
data class CategoryDescription(
    val id: String? = null,
    val name: String? = null
) : Parcelable {
    val isRoot: Boolean
        get() = id == null

    init {
        check(id == null || name != null) { "Name is required if id is given" }
    }

    companion object {
        val ROOT = CategoryDescription()
    }
}
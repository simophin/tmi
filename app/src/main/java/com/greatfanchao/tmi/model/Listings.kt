package com.greatfanchao.tmi.model

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal

data class ListingSummary @JsonCreator constructor(
    @JsonProperty("ListingId") val id: String,
    @JsonProperty("Title") val title: String,
    @JsonProperty("PhotoUrls") val photoUrls: List<String>?,
    @JsonProperty("PriceDisplay") val priceDisplay: String
)

data class ListingPhotoValue @JsonCreator constructor(
    @JsonProperty("Large")
    val url: String,

    @JsonProperty("OriginalWidth")
    val originalWidth: Int,

    @JsonProperty("OriginalHeight")
    val originalHeight: Int
)

data class ListingPhoto @JsonCreator constructor(
    @JsonProperty("Value") val value: ListingPhotoValue
) {
    val ratio: Float by lazy { value.originalWidth / value.originalHeight.toFloat() }
}

data class ListingDetails @JsonCreator constructor(
    @JsonProperty("Title") val title: String,
    @JsonProperty("Subtitle") val subtitle: String?,
    @JsonProperty("Category") val category: String,
    @JsonProperty("StartPrice") val startPrice: BigDecimal?,
    @JsonProperty("BuyNowPrice") val buyNowPrice: BigDecimal?,
    @JsonProperty("HasReserve") val hasReserve: Boolean,
    @JsonProperty("HasBuyNow") val hasBuyNow: Boolean,
    @JsonProperty("Body") val body: String,
    @JsonProperty("MaxBidAmount") val maxBidAmount: BigDecimal?,
    @JsonProperty("IsReserveMet") val reserveMet: Boolean?,
    @JsonProperty("BidCount") val bidCount: Int?,
    @JsonProperty("Photos") val photos: List<ListingPhoto>?
)
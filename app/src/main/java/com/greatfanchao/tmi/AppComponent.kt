package com.greatfanchao.tmi

import com.fasterxml.jackson.databind.ObjectMapper
import com.greatfanchao.tmi.service.AuthorisationApiService
import com.greatfanchao.tmi.service.PublicApiService

interface AppComponent {
    val publicApiService: PublicApiService
    val authorisationApiService: AuthorisationApiService
    val objectMapper: ObjectMapper
}
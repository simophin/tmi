package com.greatfanchao.tmi

import android.app.Application
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.greatfanchao.tmi.service.AuthorisationApiService
import com.greatfanchao.tmi.service.PublicApiService
import com.readystatesoftware.chuck.ChuckInterceptor
import io.reactivex.schedulers.Schedulers
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory

class App : Application(), AppComponent {
    override val publicApiService: PublicApiService by lazy {
        Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(JacksonConverterFactory.create(objectMapper))
            .client(onCreateOkHttpClient().build())
            .baseUrl(BuildConfig.ENDPOINT_BASE_URL)
            .build()
            .create(PublicApiService::class.java)
    }

    override val authorisationApiService: AuthorisationApiService by lazy {
        Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(JacksonConverterFactory.create(objectMapper))
            .client(onCreateOkHttpClient()
                .addNetworkInterceptor {
                    it.proceed(it.request()
                        .newBuilder()
                        .header("Authorization",
                            "OAuth oauth_consumer_key=\"${BuildConfig.ENDPOINT_CONSUMER_KEY}\", oauth_signature_method=\"PLAINTEXT\", oauth_signature=\"${BuildConfig.ENDPOINT_CONSUMER_SECRET}&\"")
                        .header("Content-Type", "application/x-www-form-urlencoded")
                        .build()
                    )
                }
                .build())
            .baseUrl(BuildConfig.ENDPOINT_BASE_URL)
            .build()
            .create(AuthorisationApiService::class.java)
    }

    override val objectMapper: ObjectMapper by lazy {
        ObjectMapper().apply {
            configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        }
    }

    private fun onCreateOkHttpClient(): OkHttpClient.Builder {
        return OkHttpClient.Builder()
            .addNetworkInterceptor(ChuckInterceptor(this))
            .cache(Cache(cacheDir, 10 * 1024 * 1024))
    }
}
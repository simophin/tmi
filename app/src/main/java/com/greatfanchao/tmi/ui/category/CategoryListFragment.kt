package com.greatfanchao.tmi.ui.category

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.ViewGroup
import com.greatfanchao.core.ui.fragment.BaseViewModelFragment
import com.greatfanchao.core.ui.util.ObservableListAdapter
import com.greatfanchao.core.ui.util.ViewBindingHolder
import com.greatfanchao.core.ui.util.callbackAs
import com.greatfanchao.core.ui.viewmodel.action.ViewAction
import com.greatfanchao.tmi.BR
import com.greatfanchao.tmi.R
import com.greatfanchao.tmi.databinding.FragmentLceBinding
import com.greatfanchao.tmi.databinding.ViewCategoryItemBinding
import com.greatfanchao.tmi.model.CategoryDescription
import com.greatfanchao.tmi.ui.base.RecyclerPoolCallbacks
import com.greatfanchao.tmi.ui.util.appComponent

class CategoryListFragment : BaseViewModelFragment<FragmentLceBinding, CategoryViewModel>(BR.viewModel) {
    override fun onCreateViewModel(): CategoryViewModel {
        return CategoryViewModel(
            apiService = appComponent.publicApiService,
            category = arguments!!.getParcelable(ARG_CATEGORY)
        )
    }

    override fun onCreateBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentLceBinding? {
        return FragmentLceBinding.inflate(inflater, container, false).apply {
            list.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = object : ObservableListAdapter<SubcategoryViewModel>(this@CategoryListFragment.viewModel.viewModels) {
                    override fun getLayoutResId(position: Int): Int = R.layout.view_category_item

                    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewBindingHolder<SubcategoryViewModel> {
                        return ViewBindingHolder<SubcategoryViewModel>(
                            ViewCategoryItemBinding.inflate(
                                LayoutInflater.from(parent.context),
                                parent,
                                false
                            ),
                            BR.viewModel
                        )
                    }

                    override fun onBindViewHolder(holder: ViewBindingHolder<SubcategoryViewModel>, position: Int) {
                        super.onBindViewHolder(holder, position)

                        holder.itemView.setOnClickListener {
                            if (viewModel != null) {
                                this@CategoryListFragment.viewModel.onClickCategoryItem(holder.viewModel?.categoryDescription ?: return@setOnClickListener)
                            }
                        }
                    }

                    override fun onViewRecycled(holder: ViewBindingHolder<SubcategoryViewModel>) {
                        super.onViewRecycled(holder)

                        holder.itemView.setOnClickListener(null)
                    }
                }

                addItemDecoration(DividerItemDecoration(context!!, layoutManager.layoutDirection))
                recycledViewPool = callbackAs<Callbacks>().recyclerPool
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()

        binding?.list?.adapter = null
    }

    override fun onViewAction(it: ViewAction): Boolean {
        return when (it) {
            is CategoryNavigationAction.Category -> {
                callbackAs<Callbacks>().navigateToCategory(it.category)
                true
            }
            else -> super.onViewAction(it)
        }
    }

    interface Callbacks : RecyclerPoolCallbacks {
        fun navigateToCategory(category: CategoryDescription)
    }

    companion object {
        const val ARG_CATEGORY = "category"

        fun create(category: CategoryDescription): CategoryListFragment {
            return CategoryListFragment().apply {
                arguments = Bundle(1).apply {
                    putParcelable(ARG_CATEGORY, category)
                }
            }
        }
    }
}


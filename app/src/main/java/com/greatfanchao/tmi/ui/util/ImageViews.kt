package com.greatfanchao.tmi.ui.util

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.greatfanchao.tmi.R
import com.greatfanchao.tmi.ui.widget.PlaceholderDrawable

var ImageView.url: String?
    get() = getTag(R.id.tag_image_url) as? String
    @BindingAdapter("url")
    set(value) {
        setTag(R.id.tag_image_url, value)

        val placeholder = PlaceholderDrawable(context)
        if (value != null) {
            Glide.with(this)
                    .load(url)
                    .apply(RequestOptions.errorOf(placeholder))
                    .apply(RequestOptions.placeholderOf(placeholder))
                    .into(this)
        } else {
            setImageDrawable(placeholder)
            Glide.with(this).clear(this)
        }
    }
package com.greatfanchao.tmi.ui.category

import com.greatfanchao.core.ui.viewmodel.action.ViewAction
import com.greatfanchao.tmi.model.CategoryDescription

sealed class CategoryNavigationAction : ViewAction {
    data class Category(val category: CategoryDescription) : CategoryNavigationAction()
    object ScrollToTop : CategoryNavigationAction()
}
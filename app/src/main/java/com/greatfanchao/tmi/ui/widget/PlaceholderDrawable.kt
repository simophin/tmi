package com.greatfanchao.tmi.ui.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable
import com.greatfanchao.core.ui.util.getColorCompat
import com.greatfanchao.core.ui.util.getDrawableCompat
import com.greatfanchao.tmi.R

class PlaceholderDrawable(context: Context) : Drawable() {
    private val bgColor: Int = context.getColorCompat(R.color.md_grey300)
    private val fg = context.getDrawableCompat(R.drawable.ic_image_grey_24dp)

    override fun draw(canvas: Canvas) {
        canvas.drawColor(bgColor)
        val left = bounds.left + (bounds.width() - fg.intrinsicWidth) / 2
        val top = bounds.top + (bounds.height() - fg.intrinsicHeight) / 2
        fg.setBounds(left, top, left + fg.intrinsicWidth, top + fg.intrinsicHeight)
        fg.draw(canvas)
    }

    override fun setAlpha(alpha: Int) {}

    override fun getOpacity(): Int {
        return PixelFormat.OPAQUE
    }

    override fun setColorFilter(filter: ColorFilter) {}
}
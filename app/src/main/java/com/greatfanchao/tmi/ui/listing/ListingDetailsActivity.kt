package com.greatfanchao.tmi.ui.listing

import android.os.Bundle
import com.greatfanchao.tmi.ui.base.BaseActivity

class ListingDetailsActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(android.R.id.content, ListingDetailsFragment.create(intent.getStringExtra(EXTRA_LISTING_ID)))
                .commit()
        }
    }

    companion object {
        const val EXTRA_LISTING_ID = "id"
    }
}
package com.greatfanchao.tmi.ui.category

import android.databinding.ObservableList
import android.os.Bundle
import com.greatfanchao.core.ui.util.observe
import com.greatfanchao.core.ui.viewmodel.ActionableViewModel
import com.greatfanchao.core.ui.viewmodel.ListViewModel
import com.greatfanchao.core.ui.viewmodel.StatefulViewModel
import com.greatfanchao.core.ui.viewmodel.ViewModel
import com.greatfanchao.core.ui.viewmodel.action.ViewAction
import com.greatfanchao.tmi.model.CategoryDescription
import com.greatfanchao.tmi.ui.util.ObservableArrayList
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class CategoryBreadcrumbsViewModel :  ViewModel, StatefulViewModel, ListViewModel<CategoryBreadcrumbItemViewModel>, ActionableViewModel {
    private val categoryHierarchy = ObservableArrayList<CategoryBreadcrumbItemViewModel>().apply {
        add(CategoryBreadcrumbItemViewModel(CategoryDescription.ROOT))
    }

    override val viewModels: ObservableList<CategoryBreadcrumbItemViewModel>
        get() = categoryHierarchy

    val currentCategory: Observable<CategoryDescription> = viewModels.observe().map { it.last().category }

    override val viewActions = PublishSubject.create<ViewAction>()

    override fun onRestoreState(inState: Bundle) {
        categoryHierarchy.replaceAll(inState.getParcelableArrayList(STATE_HIERARCHY), calcDiff = false)
    }

    override fun onSaveState(outState: Bundle) {
        outState.putParcelableArrayList(STATE_HIERARCHY, categoryHierarchy)
    }

    fun requestUp(): Boolean {
        if (categoryHierarchy.size <= 1) {
            return false
        }

        categoryHierarchy.removeAt(categoryHierarchy.size - 1)
        return true
    }

    fun requestEnterCategory(newCategory: CategoryDescription) {
        val existingIndex = categoryHierarchy.indexOfLast { it.category == newCategory }
        if (existingIndex >= 0 && existingIndex == categoryHierarchy.size - 1) {
            viewActions.onNext(CategoryNavigationAction.ScrollToTop)
            return
        }

        val origSize = categoryHierarchy.size
        if (existingIndex >= 0 && existingIndex < origSize - 1) {
            // If this category is already in the hierarchy, pop the hierarchy to that level
            categoryHierarchy.removeRange(existingIndex + 1, origSize)
        }
        else {
            // Category doesn't exist, add to hierarchy
            categoryHierarchy.add(CategoryBreadcrumbItemViewModel(newCategory))
        }
    }

    fun requestExitCategory(category: CategoryDescription) {
        val existingIndex = categoryHierarchy.indexOfLast { it.category == category }
        if (existingIndex >= 0) {
            categoryHierarchy.removeRange(existingIndex, categoryHierarchy.size)
        }
    }

    fun onCategoryBreadcrumbClick(category: CategoryBreadcrumbItemViewModel) {
        requestEnterCategory(category.category)
    }

    companion object {
        private const val STATE_HIERARCHY = "hierarchy"

    }
}
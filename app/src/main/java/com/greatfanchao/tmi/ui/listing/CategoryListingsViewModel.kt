package com.greatfanchao.tmi.ui.listing

import com.greatfanchao.core.ui.util.doOnLoading
import com.greatfanchao.core.ui.viewmodel.ActionableViewModel
import com.greatfanchao.core.ui.viewmodel.EmptyViewModel
import com.greatfanchao.core.ui.viewmodel.ErrorViewModel
import com.greatfanchao.core.ui.viewmodel.LceViewModel
import com.greatfanchao.core.ui.viewmodel.ListViewModel
import com.greatfanchao.core.ui.viewmodel.LoadingViewModel
import com.greatfanchao.core.ui.viewmodel.ViewModel
import com.greatfanchao.core.ui.viewmodel.action.ViewAction
import com.greatfanchao.core.ui.viewmodel.impl.EmptyViewModelImpl
import com.greatfanchao.core.ui.viewmodel.impl.ErrorViewModelImpl
import com.greatfanchao.core.ui.viewmodel.impl.LifecycleViewModelImpl
import com.greatfanchao.core.ui.viewmodel.impl.LoadingViewModelImpl
import com.greatfanchao.tmi.model.CategoryDescription
import com.greatfanchao.tmi.service.AuthorisationApiService
import com.greatfanchao.tmi.service.PublicApiService
import com.greatfanchao.tmi.ui.category.CategoryNavigationAction
import com.greatfanchao.tmi.ui.category.SubcategoryViewModel
import com.greatfanchao.tmi.ui.util.ObservableArrayList
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.Singles
import io.reactivex.subjects.PublishSubject

class CategoryListingsViewModel(
    val categoryId: String,
    private val apiService: AuthorisationApiService,
    private val publicApiService: PublicApiService,
    private val loadSubcategories: Boolean = true
) : LifecycleViewModelImpl(), LceViewModel, ActionableViewModel, ListViewModel<ViewModel> {

    private val subcategoriesViewModel = SubcategoriesViewModel()

    override val viewActions: PublishSubject<ViewAction> = PublishSubject.create()
    override val viewModels = ObservableArrayList<ViewModel>()

    private var loadListingsDisposable: Disposable? = null

    override val errorViewModel: ErrorViewModel = object : ErrorViewModelImpl() {
        override fun onRetry() {
            super.onRetry()
            loadCategoryAndListings()
        }
    }
    override val loadingViewModel: LoadingViewModel = LoadingViewModelImpl()
    override val emptyViewModel: EmptyViewModel = EmptyViewModelImpl()

    override fun onStart() {
        super.onStart()

        loadCategoryAndListings()
    }

    fun onClickSummaryItem(id: String) {
        viewActions.onNext(ListingNavigationAction.ListingDetails(id))
    }

    fun onClickSubcategoryItem(category: CategoryDescription) {
        viewActions.onNext(CategoryNavigationAction.Category(category))
    }

    private fun loadCategoryAndListings() {
        loadListingsDisposable?.dispose()

        val subcategoriesSubject = if (loadSubcategories) publicApiService.retrieveCategory(categoryId).map {
            it.subcategories ?: emptyList()
        } else Single.just(emptyList())

        loadListingsDisposable = Singles.zip(
            apiService.retrieveListingsByCategory(categoryId),
            subcategoriesSubject) { listings, category -> (listings.data ?: emptyList()) to category }
            .doOnLoading {
                loadingViewModel.isLoading.set(it)
                if (it) {
                    emptyViewModel.isEmpty.set(false)
                }
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ (listings, categories) ->
                subcategoriesViewModel.viewModels.replaceAll(categories.map { SubcategoryViewModel(it) })
                val finalViewModels = ArrayList<ViewModel>(1 + listings.size)
                if (subcategoriesViewModel.viewModels.isNotEmpty()) {
                    finalViewModels.add(subcategoriesViewModel)
                }

                viewModels.replaceAll(listings.mapTo(finalViewModels, { ListingSummaryItemViewModel(it) }))
                emptyViewModel.isEmpty.set(listings.isEmpty() && categories.isEmpty())
            }, {
                errorViewModel.error.set(it)
                emptyViewModel.isEmpty.set(false)
            })
            .bindToLifecycle()
    }
}
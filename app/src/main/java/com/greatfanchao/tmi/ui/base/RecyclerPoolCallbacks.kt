package com.greatfanchao.tmi.ui.base

import android.support.v7.widget.RecyclerView

/**
 * A generic callback that provides a [android.support.v7.widget.RecyclerView.RecycledViewPool]
 */
interface RecyclerPoolCallbacks {
    val recyclerPool: RecyclerView.RecycledViewPool
}
package com.greatfanchao.tmi.ui.category

import android.os.Parcelable
import com.greatfanchao.core.ui.viewmodel.ViewModel
import com.greatfanchao.tmi.model.CategoryDescription
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CategoryBreadcrumbItemViewModel(val category: CategoryDescription) : ViewModel, Parcelable {
    val text: String?
        get() = category.name
}
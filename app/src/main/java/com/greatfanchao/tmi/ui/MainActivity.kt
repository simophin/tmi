package com.greatfanchao.tmi.ui

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.RecyclerView
import com.greatfanchao.tmi.R
import com.greatfanchao.tmi.databinding.ActivityMainBinding
import com.greatfanchao.tmi.model.CategoryDescription
import com.greatfanchao.tmi.ui.base.BaseActivity
import com.greatfanchao.tmi.ui.base.RecyclerPoolCallbacks
import com.greatfanchao.tmi.ui.category.CategoryBreadcrumbsFragment
import com.greatfanchao.tmi.ui.category.CategoryListFragment
import com.greatfanchao.tmi.ui.listing.CategoryListingsFragment
import com.greatfanchao.tmi.ui.listing.ListingDetailsActivity

class MainActivity : BaseActivity(),
    RecyclerPoolCallbacks,
    CategoryListFragment.Callbacks,
    CategoryListingsFragment.Callbacks,
    CategoryBreadcrumbsFragment.Callbacks {

    override val recyclerPool: RecyclerView.RecycledViewPool = RecyclerView.RecycledViewPool()

    private lateinit var binding: ActivityMainBinding

    private val categoryBreadcrumbsFragment: CategoryBreadcrumbsFragment
        get() = (supportFragmentManager.findFragmentById(R.id.breadcrumbs) as CategoryBreadcrumbsFragment)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.breadcrumbs, CategoryBreadcrumbsFragment())
                .commit()
        }
    }

    override fun onStart() {
        super.onStart()

        categoryBreadcrumbsFragment.viewModel.currentCategory
            .distinctUntilChanged(CategoryDescription::id)
            .subscribe {
                val currentBrowseFragment = supportFragmentManager.findFragmentById(binding.browseContainer.id)
                val newFragment = if (it.isRoot && currentBrowseFragment !is CategoryListFragment) {
                    CategoryListFragment.create(CategoryDescription.ROOT)
                } else if (!it.isRoot && (currentBrowseFragment !is CategoryListingsFragment || currentBrowseFragment.viewModel.categoryId != it.id)) {
                    CategoryListingsFragment.create(it.id!!)
                } else {
                    null
                }

                if (newFragment != null) {
                    supportFragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .replace(binding.browseContainer.id, newFragment)
                        .commitNow()
                }
            }
            .bindToLifecycle()
    }

    override fun navigateToPageTop() {
        val currentBrowseFragment = supportFragmentManager.findFragmentById(binding.browseContainer.id)
        if (currentBrowseFragment is CategoryListingsFragment) {
            currentBrowseFragment.scrollToTop()
        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.popBackStackImmediate()) {
            return
        }

        if (!categoryBreadcrumbsFragment.viewModel.requestUp()) {
            super.onBackPressed()
        }
    }

    override fun navigateToListingDetails(listingId: String) {
        startActivity(Intent(this, ListingDetailsActivity::class.java)
            .putExtra(ListingDetailsActivity.EXTRA_LISTING_ID, listingId))
    }

    override fun navigateToCategory(category: CategoryDescription) {
        categoryBreadcrumbsFragment.viewModel.requestEnterCategory(category)
    }
}

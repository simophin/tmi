package com.greatfanchao.tmi.ui.listing

import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.databinding.ObservableFloat
import android.databinding.ObservableInt
import android.util.Log
import com.greatfanchao.core.ui.util.doOnLoading
import com.greatfanchao.core.ui.viewmodel.LceViewModel
import com.greatfanchao.core.ui.viewmodel.impl.EmptyViewModelImpl
import com.greatfanchao.core.ui.viewmodel.impl.ErrorViewModelImpl
import com.greatfanchao.core.ui.viewmodel.impl.LifecycleViewModelImpl
import com.greatfanchao.core.ui.viewmodel.impl.LoadingViewModelImpl
import com.greatfanchao.tmi.C
import com.greatfanchao.tmi.service.AuthorisationApiService
import com.greatfanchao.tmi.ui.util.ObservableArrayList
import io.reactivex.android.schedulers.AndroidSchedulers
import java.math.BigDecimal

class ListingDetailsViewModel(
    private val listingId: String,
    private val apiService: AuthorisationApiService
) : LifecycleViewModelImpl(), LceViewModel {
    val photos = ObservableArrayList<String>()
    val name = ObservableField<String>()
    val categoryName = ObservableField<String>()
    val description = ObservableField<String>()

    val hasStartPrice = ObservableBoolean()
    val startPrice = ObservableField<String>()

    val hasBuyNow = ObservableBoolean()
    val buyNowPrice = ObservableField<String>()

    val isBidding = ObservableBoolean()
    val maxBidAmount = ObservableField<String>()
    val hasMaxBidAmount = ObservableBoolean()

    val hasReserve = ObservableBoolean()
    val reserveMet = ObservableBoolean()
    val numBids = ObservableInt()

    override val emptyViewModel = EmptyViewModelImpl()
    override val errorViewModel = object : ErrorViewModelImpl() {
        override fun onRetry() {
            loadListingDetails()
        }
    }
    override val loadingViewModel = LoadingViewModelImpl()


    override fun onStart() {
        super.onStart()

        loadListingDetails()
    }

    private fun loadListingDetails() {
        apiService.retrieveListingDetails(listingId)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnLoading { loadingViewModel.isLoading.set(it) }
            .subscribe({
                errorViewModel.error.set(null)

                val photos = it.photos ?: emptyList()
                this.photos.replaceAll(photos.map { it.value.url }, calcDiff = false)
                name.set(it.title)
                categoryName.set(it.category)
                description.set(it.body)
                hasStartPrice.set(it.startPrice != null && it.startPrice >= BigDecimal.ONE)
                startPrice.set(it.startPrice?.let { C.MONEY_FORMAT_WITH_SYMBOL.format(it) })
                hasBuyNow.set(it.hasBuyNow)
                buyNowPrice.set(it.buyNowPrice?.let { C.MONEY_FORMAT_WITH_SYMBOL.format(it) })
                isBidding.set(it.startPrice != null) //TODO: is it correct?
                maxBidAmount.set(it.maxBidAmount?.let { C.MONEY_FORMAT_WITH_SYMBOL.format(it) })
                hasMaxBidAmount.set(it.maxBidAmount != null)
                hasReserve.set(it.hasReserve)
                reserveMet.set(it.reserveMet ?: false)
                numBids.set(it.bidCount ?: 0)

            }, {
                Log.e("ListingDetails", "Error retrieving listing", it)
                errorViewModel.error.set(it)
            })
            .bindToLifecycle()
    }
}
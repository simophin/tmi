package com.greatfanchao.tmi.ui.widget

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import com.google.android.flexbox.FlexboxLayout
import com.greatfanchao.core.ui.util.ViewGroupRecycler

class RecyclingFlexboxLayout @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyle: Int = 0) : FlexboxLayout(context, attributeSet, defStyle) {

    private val recycler = ViewGroupRecycler(this)

    var adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>?
        set(value) {
            recycler.adapter = value
        }
        get() = recycler.adapter
}
package com.greatfanchao.tmi.ui.util

import android.support.v4.app.Fragment
import com.greatfanchao.tmi.AppComponent

val Fragment.appComponent: AppComponent
    get() = context!!.applicationContext as AppComponent
package com.greatfanchao.tmi.ui.category

import android.databinding.ObservableList
import com.greatfanchao.core.ui.util.doOnLoading
import com.greatfanchao.core.ui.viewmodel.ActionableViewModel
import com.greatfanchao.core.ui.viewmodel.EmptyViewModel
import com.greatfanchao.core.ui.viewmodel.ErrorViewModel
import com.greatfanchao.core.ui.viewmodel.LceViewModel
import com.greatfanchao.core.ui.viewmodel.ListViewModel
import com.greatfanchao.core.ui.viewmodel.LoadingViewModel
import com.greatfanchao.core.ui.viewmodel.action.ViewAction
import com.greatfanchao.core.ui.viewmodel.impl.EmptyViewModelImpl
import com.greatfanchao.core.ui.viewmodel.impl.ErrorViewModelImpl
import com.greatfanchao.core.ui.viewmodel.impl.LifecycleViewModelImpl
import com.greatfanchao.core.ui.viewmodel.impl.LoadingViewModelImpl
import com.greatfanchao.tmi.model.CategoryDescription
import com.greatfanchao.tmi.service.NoSuchCategoryException
import com.greatfanchao.tmi.service.PublicApiService
import com.greatfanchao.tmi.ui.util.ObservableArrayList
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject

class CategoryViewModel(
        private val apiService: PublicApiService,
        val category: CategoryDescription
) : LifecycleViewModelImpl(), LceViewModel, ActionableViewModel, ListViewModel<SubcategoryViewModel> {

    // Private states
    private val subcategories = ObservableArrayList<SubcategoryViewModel>()

    // Observable view states
    val categoryName: String?
        get() = category.name

    override val viewActions: PublishSubject<ViewAction> = PublishSubject.create()

    // LCE/List ViewModel components
    override val errorViewModel: ErrorViewModel = object : ErrorViewModelImpl() {
        override fun onRetry() {
            loadCategoryData()
        }
    }
    override val loadingViewModel: LoadingViewModel = LoadingViewModelImpl()
    override val emptyViewModel: EmptyViewModel = EmptyViewModelImpl()
    override val viewModels: ObservableList<SubcategoryViewModel>
        get() = subcategories

    private var loadCategoryDisposable: Disposable? = null

    override fun onStart() {
        super.onStart()

        loadCategoryData()
    }

    private fun loadCategoryData() {
        loadCategoryDisposable?.dispose()

        val categorySubject = if (category.id == null) {
            apiService.retrieveRootCategory()
        } else {
            apiService.retrieveCategory(category.id)
        }

        loadCategoryDisposable = categorySubject
            .map { category ->
                category.subcategories?.map { SubcategoryViewModel(it) } ?: emptyList()
            }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnLoading {
                loadingViewModel.isLoading.set(it && viewModels.isEmpty())
                if (it) {
                    emptyViewModel.isEmpty.set(false)
                }
            }
            .subscribe({ categoryViewModels ->
                if (categoryViewModels != subcategories) {
                    subcategories.replaceAll(categoryViewModels)
                    emptyViewModel.isEmpty.set(categoryViewModels.isEmpty())
                }
            }, { err ->
                emptyViewModel.isEmpty.set(false)
                // Transform err
                errorViewModel.error.set(when (err) {
                    is NoSuchElementException -> NoSuchCategoryException(category.id)
                    else -> err
                })
            })
            .bindToLifecycle()
    }

    fun onClickCategoryItem(category: CategoryDescription) {
        viewActions.onNext(CategoryNavigationAction.Category(category))
    }
}
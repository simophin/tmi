package com.greatfanchao.tmi.ui.listing

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.view.PagerAdapter
import android.support.v7.widget.AppCompatImageView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.greatfanchao.core.ui.fragment.BaseViewModelFragment
import com.greatfanchao.core.ui.util.observe
import com.greatfanchao.tmi.BR
import com.greatfanchao.tmi.databinding.FragmentListingDetailsBinding
import com.greatfanchao.tmi.ui.util.appComponent
import com.greatfanchao.tmi.ui.util.url
import com.greatfanchao.tmi.ui.widget.PlaceholderDrawable

class ListingDetailsFragment : BaseViewModelFragment<FragmentListingDetailsBinding, ListingDetailsViewModel>(BR.viewModel) {
    override fun onCreateViewModel(): ListingDetailsViewModel {
        return ListingDetailsViewModel(
            listingId = arguments!!.getString(ARG_LISTING_ID),
            apiService = appComponent.authorisationApiService
        )
    }

    override fun onCreateBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentListingDetailsBinding? {
        return FragmentListingDetailsBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.apply {
            val adapter = PhotoAdapter()
            photos.adapter = adapter
            indicator.setViewPager(photos)
            adapter.registerDataSetObserver(indicator.dataSetObserver)

            this@ListingDetailsFragment.viewModel.photos
                .observe()
                .subscribe {
                    adapter.photos = it
                    if (it.isEmpty()) {
                        photos.background = PlaceholderDrawable(photos.context)
                    } else {
                        photos.background = ColorDrawable(Color.BLACK)
                    }
                }
        }
    }

    private class PhotoAdapter : PagerAdapter() {
        var photos: List<String> = emptyList()
            set(value) {
                if (field != value) {
                    field = value
                    notifyDataSetChanged()
                }
            }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            return AppCompatImageView(container.context).apply {
                scaleType = ImageView.ScaleType.FIT_CENTER
                adjustViewBounds = false
                url = photos[position]
                container.addView(this, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            }
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            (`object` as ImageView).url = null
            container.removeView(`object`)
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view == `object`
        }

        override fun getCount(): Int {
            return photos.size
        }
    }

    companion object {
        const val ARG_LISTING_ID = "id"

        fun create(id: String): ListingDetailsFragment {
            return ListingDetailsFragment().apply {
                arguments = Bundle(1).apply {
                    putString(ARG_LISTING_ID, id)
                }
            }
        }
    }
}
package com.greatfanchao.tmi.ui.listing

import com.greatfanchao.core.ui.viewmodel.ListViewModel
import com.greatfanchao.core.ui.viewmodel.ViewModel
import com.greatfanchao.tmi.ui.category.SubcategoryViewModel
import com.greatfanchao.tmi.ui.util.ObservableArrayList

class SubcategoriesViewModel : ViewModel, ListViewModel<SubcategoryViewModel> {
    override val viewModels = ObservableArrayList<SubcategoryViewModel>()
}
package com.greatfanchao.tmi.ui.listing

import com.greatfanchao.core.ui.viewmodel.ViewModel
import com.greatfanchao.tmi.C
import com.greatfanchao.tmi.model.ListingSummary

data class ListingSummaryItemViewModel(val listing: ListingSummary) : ViewModel {
    val title: String
        get() = listing.title

    val imageUrl: String?
        get() = listing.photoUrls?.firstOrNull()

    val price: String
        get() = listing.priceDisplay
}
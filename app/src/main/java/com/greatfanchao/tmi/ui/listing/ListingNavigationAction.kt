package com.greatfanchao.tmi.ui.listing

import com.greatfanchao.core.ui.viewmodel.action.ViewAction

sealed class ListingNavigationAction : ViewAction {
    data class ListingDetails(val id: String) : ListingNavigationAction()
}
package com.greatfanchao.tmi.ui.category

import android.content.Context
import android.graphics.Canvas
import android.graphics.PorterDuff
import android.graphics.Rect
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.greatfanchao.core.ui.fragment.BaseViewModelFragment
import com.greatfanchao.core.ui.util.ObservableListAdapter
import com.greatfanchao.core.ui.util.ViewBindingHolder
import com.greatfanchao.core.ui.util.callbackAs
import com.greatfanchao.core.ui.util.getDrawableCompat
import com.greatfanchao.core.ui.viewmodel.action.ViewAction
import com.greatfanchao.tmi.BR
import com.greatfanchao.tmi.R
import com.greatfanchao.tmi.databinding.FragmentListBinding
import com.greatfanchao.tmi.databinding.ViewCategoryBreadcrumbItemBinding
import com.greatfanchao.tmi.ui.base.RecyclerPoolCallbacks
import kotlin.math.roundToInt

class CategoryBreadcrumbsFragment : BaseViewModelFragment<FragmentListBinding, CategoryBreadcrumbsViewModel>() {
    override fun onCreateBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentListBinding? {
        return FragmentListBinding.inflate(inflater, container, false).apply {
            list.apply {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                adapter = Adapter()
                recycledViewPool = callbackAs<Callbacks>().recyclerPool
                addItemDecoration(Decoration(context))
            }
        }
    }

    override fun onCreateViewModel(): CategoryBreadcrumbsViewModel {
        return CategoryBreadcrumbsViewModel().apply {
            currentCategory
                .subscribe {
                    // Whenever current category changes, always scrolls to the end of the list so we can see it
                    binding?.list?.apply {
                        smoothScrollToPosition(adapter.itemCount - 1)
                    }
                }
        }
    }

    override fun onViewAction(it: ViewAction): Boolean {
        if (it == CategoryNavigationAction.ScrollToTop) {
            callbackAs<Callbacks>().navigateToPageTop()
            return true
        }

        return super.onViewAction(it)
    }

    private inner class Adapter : ObservableListAdapter<CategoryBreadcrumbItemViewModel>(viewModel.viewModels) {
        override fun getLayoutResId(position: Int): Int {
            return R.layout.view_category_breadcrumb_item
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewBindingHolder<CategoryBreadcrumbItemViewModel> {
            return ViewBindingHolder<CategoryBreadcrumbItemViewModel>(
                ViewCategoryBreadcrumbItemBinding.inflate(LayoutInflater.from(parent.context), parent, false),
                BR.viewModel
            ).also { holder ->
                holder.itemView.setOnClickListener {
                    viewModel.onCategoryBreadcrumbClick(holder.viewModel ?: return@setOnClickListener)
                }
            }
        }
    }

    /**
     * Draw arrow on the right of each breadcrumb except last one.
     */
    private class Decoration(context: Context) : RecyclerView.ItemDecoration() {
        private val arrowDrawable = context.getDrawableCompat(R.drawable.ic_chevron_right_black_24dp)

        override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
            for (i in 0 until parent.childCount) {
                val child = parent.getChildAt(i)
                val holder = parent.findContainingViewHolder(child)
                if (holder != null && holder.adapterPosition == 0) {
                    // Don't draw anything on first item
                    continue
                }

                val top = ((child.height - arrowDrawable.intrinsicHeight) / 2f + child.translationY).roundToInt()
                val left = (child.left - arrowDrawable.intrinsicWidth + child.translationX).roundToInt()
                Log.d("Breadcrumbs", "child.adapterPosition=${parent.findContainingViewHolder(child)?.adapterPosition}, left=$left")
                arrowDrawable.setBounds(left, top, left + arrowDrawable.intrinsicWidth, top + arrowDrawable.intrinsicHeight)
                if (child is TextView) {
                    arrowDrawable.setColorFilter(child.currentTextColor, PorterDuff.Mode.MULTIPLY)
                }
                arrowDrawable.draw(c)
            }
        }

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            val holder = parent.findContainingViewHolder(view) ?: return
            if (holder.adapterPosition != 0) {
                // Make space at left of items (except the first one) for the arrow
                outRect.left = arrowDrawable.intrinsicWidth
            }

        }
    }

    interface Callbacks : RecyclerPoolCallbacks {
        fun navigateToPageTop()
    }
}
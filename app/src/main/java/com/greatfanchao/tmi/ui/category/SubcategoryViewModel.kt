package com.greatfanchao.tmi.ui.category

import com.greatfanchao.core.ui.viewmodel.ViewModel
import com.greatfanchao.tmi.model.Category
import com.greatfanchao.tmi.model.CategoryDescription

data class SubcategoryViewModel(
        private val category: Category
) : ViewModel {
    val categoryDescription: CategoryDescription
        get() = category.toDescription()

    val name: String
        get() = category.name

    val showRightArrow: Boolean
        get() = !category.leaf
}
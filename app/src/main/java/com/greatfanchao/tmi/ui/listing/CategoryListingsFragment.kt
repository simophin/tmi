package com.greatfanchao.tmi.ui.listing

import android.databinding.ObservableList
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.greatfanchao.core.ui.fragment.BaseViewModelFragment
import com.greatfanchao.core.ui.util.ObservableListAdapter
import com.greatfanchao.core.ui.util.ViewBindingHolder
import com.greatfanchao.core.ui.util.callbackAs
import com.greatfanchao.core.ui.viewmodel.ViewModel
import com.greatfanchao.core.ui.viewmodel.action.ViewAction
import com.greatfanchao.tmi.BR
import com.greatfanchao.tmi.R
import com.greatfanchao.tmi.databinding.FragmentLceBinding
import com.greatfanchao.tmi.databinding.ViewListingSubcategoriesBinding
import com.greatfanchao.tmi.databinding.ViewListingSubcategoryItemBinding
import com.greatfanchao.tmi.databinding.ViewListingSummaryItemBinding
import com.greatfanchao.tmi.model.CategoryDescription
import com.greatfanchao.tmi.ui.base.RecyclerPoolCallbacks
import com.greatfanchao.tmi.ui.category.CategoryNavigationAction
import com.greatfanchao.tmi.ui.category.SubcategoryViewModel
import com.greatfanchao.tmi.ui.util.appComponent
import io.reactivex.functions.Consumer
import java.util.concurrent.atomic.AtomicInteger

class CategoryListingsFragment : BaseViewModelFragment<FragmentLceBinding, CategoryListingsViewModel>(BR.viewModel) {
    override fun onCreateBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentLceBinding? {
        return FragmentLceBinding.inflate(inflater, container, false)
    }

    override fun onCreateViewModel(): CategoryListingsViewModel {
        return CategoryListingsViewModel(
            categoryId = arguments!!.getString(ARG_INITIAL_CATEGORY_ID),
            apiService = appComponent.authorisationApiService,
            publicApiService = appComponent.publicApiService,
            loadSubcategories = true
        )
    }

    override fun onViewAction(it: ViewAction): Boolean {
        return when (it) {
            is ListingNavigationAction.ListingDetails -> {
                callbackAs<Callbacks>().navigateToListingDetails(it.id)
                true
            }
            is CategoryNavigationAction.Category -> {
                callbackAs<Callbacks>().navigateToCategory(it.category)
                true
            }
            else -> super.onViewAction(it)
        }
    }

    override fun onViewBindingCreated(binding: FragmentLceBinding) {
        super.onViewBindingCreated(binding)

        binding.list.apply {
            val columnWidth = resources.getDimensionPixelSize(R.dimen.column_width)
            val minColumnSpacing = resources.getDimensionPixelSize(R.dimen.min_column_spacing)
            val columnSpacing = AtomicInteger(minColumnSpacing)

            setPadding(minColumnSpacing, paddingTop, minColumnSpacing, paddingBottom)

            layoutManager = object : GridLayoutManager(context, 1) {
                override fun onMeasure(recycler: RecyclerView.Recycler?, state: RecyclerView.State?, widthSpec: Int, heightSpec: Int) {
                    super.onMeasure(recycler, state, widthSpec, heightSpec)

                    spanCount = (measuredWidth - minColumnSpacing) / (minColumnSpacing + columnWidth)
                    columnSpacing.set((measuredWidth - spanCount * columnWidth) / (spanCount + 1))
                    check(columnSpacing.get() >= minColumnSpacing)
                }
            }.apply {
                spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                    override fun getSpanSize(position: Int): Int {
                        return if (adapter.getItemViewType(position) == R.layout.view_listing_subcategories) spanCount else 1
                    }
                }
            }

            recycledViewPool = callbackAs<Callbacks>().recyclerPool

            adapter = ListingAdapter()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()

        // Release views to the pool
        binding?.list?.adapter = null
    }

    fun scrollToTop() {
        binding?.list?.smoothScrollToPosition(0)
    }

    private class SubcategoriesAdapter(list: ObservableList<SubcategoryViewModel>) : ObservableListAdapter<SubcategoryViewModel>(list) {
        var onSubcategoryItemClickListener: Consumer<CategoryDescription>? = null

        override fun getLayoutResId(position: Int): Int = R.layout.view_listing_subcategory_item

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewBindingHolder<SubcategoryViewModel> {
            return ViewBindingHolder(
                binding = ViewListingSubcategoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false),
                viewModelBindingVariableId = BR.viewModel
            )
        }

        override fun onBindViewHolder(holder: ViewBindingHolder<SubcategoryViewModel>, position: Int) {
            super.onBindViewHolder(holder, position)

            holder.itemView.setOnClickListener { _->
                onSubcategoryItemClickListener?.accept(holder.viewModel?.categoryDescription ?: return@setOnClickListener)
            }
        }
    }

    private inner class ListingAdapter : ObservableListAdapter<ViewModel>(viewModel.viewModels) {

        override fun getLayoutResId(position: Int): Int {
            return when (list[position]) {
                is ListingSummaryItemViewModel -> R.layout.view_listing_summary_item
                is SubcategoriesViewModel -> R.layout.view_listing_subcategories
                else -> throw RuntimeException("Unknown ViewModel ${list[position]}")
            }
        }

        @Suppress("UNCHECKED_CAST")
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewBindingHolder<ViewModel> {
            return when (viewType) {
                R.layout.view_listing_subcategories -> {
                    ViewBindingHolder<SubcategoryViewModel>(
                        binding = ViewListingSubcategoriesBinding.inflate(LayoutInflater.from(parent.context), parent, false),
                        viewModelBindingVariableId = BR.viewModel
                    ) as ViewBindingHolder<ViewModel>
                }

                R.layout.view_listing_summary_item -> {
                    ViewBindingHolder<ListingSummaryItemViewModel>(
                        binding = ViewListingSummaryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false),
                        viewModelBindingVariableId = BR.viewModel
                    ) as ViewBindingHolder<ViewModel>
                }

                else -> throw RuntimeException("Unknown ViewType: $viewType")
            }
        }

        override fun onBindViewHolder(holder: ViewBindingHolder<ViewModel>, position: Int) {
            super.onBindViewHolder(holder, position)

            holder.binding.apply {
                when (this) {
                    is ViewListingSummaryItemBinding -> {
                        root.setOnClickListener {
                            this@CategoryListingsFragment.viewModel.onClickSummaryItem(viewModel?.listing?.id ?: return@setOnClickListener)
                        }
                    }

                    is ViewListingSubcategoriesBinding -> {
                        val adapter = categoryContainer.adapter as? SubcategoriesAdapter
                        val viewModel = list[position] as SubcategoriesViewModel
                        if (adapter == null || adapter.list !== viewModel.viewModels) {
                            @Suppress("UNCHECKED_CAST")
                            categoryContainer.adapter = SubcategoriesAdapter(viewModel.viewModels) as RecyclerView.Adapter<RecyclerView.ViewHolder>
                        }

                        (categoryContainer.adapter as SubcategoriesAdapter).onSubcategoryItemClickListener = Consumer {
                            this@CategoryListingsFragment.viewModel.onClickSubcategoryItem(it)
                        }
                    }
                }
            }
        }

        override fun onViewRecycled(holder: ViewBindingHolder<ViewModel>) {
            super.onViewRecycled(holder)

            if (holder.binding is ViewListingSubcategoriesBinding) {
                // Clear subcategory listener if there is one
                ((holder.binding as? ViewListingSubcategoriesBinding)?.categoryContainer?.adapter as? SubcategoriesAdapter)?.onSubcategoryItemClickListener = null
            }
            else {
                holder.itemView.setOnClickListener(null)
            }
        }
    }

    interface Callbacks : RecyclerPoolCallbacks {
        fun navigateToListingDetails(listingId: String)
        fun navigateToCategory(category: CategoryDescription)
    }

    companion object {
        const val ARG_INITIAL_CATEGORY_ID = "cat_id"

        fun create(initialCategoryId: String): Fragment {
            return CategoryListingsFragment().apply {
                arguments = Bundle(1).apply {
                    putString(ARG_INITIAL_CATEGORY_ID, initialCategoryId)
                }
            }
        }
    }
}
package com.greatfanchao.tmi.ui.base

import android.support.v7.app.AppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseActivity : AppCompatActivity() {
    private var disposable: CompositeDisposable? = null

    override fun onStop() {
        super.onStop()

        disposable?.dispose()
        disposable = null
    }

    fun Disposable.bindToLifecycle(): Disposable {
        if (disposable == null) {
            disposable = CompositeDisposable(this)
        }
        else {
            disposable!!.add(this)
        }

        return this
    }
}
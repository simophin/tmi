package com.greatfanchao.tmi.ui.util

import android.databinding.ListChangeRegistry
import android.databinding.ObservableList
import android.support.v7.util.DiffUtil
import android.support.v7.util.ListUpdateCallback

class ObservableArrayList<T> : ArrayList<T>(), ObservableList<T> {
    @Transient
    private var updateCallbacks : ListUpdateCallback? = null

    @Transient
    private var listeners: ListChangeRegistry? = null

    override fun addOnListChangedCallback(callback: ObservableList.OnListChangedCallback<out ObservableList<T>>?) {
        if (listeners == null) {
            listeners = ListChangeRegistry()
        }
        listeners!!.add(callback)
    }

    override fun removeOnListChangedCallback(callback: ObservableList.OnListChangedCallback<out ObservableList<T>>?) {
        if (listeners != null) {
            listeners!!.remove(callback)
        }
    }

    override fun add(element: T): Boolean {
        super.add(element)
        notifyAdd(size - 1, 1)
        return true
    }

    override fun add(index: Int, element: T) {
        super.add(index, element)
        notifyAdd(index, 1)
    }

    override fun addAll(elements: Collection<T>): Boolean {
        val oldSize = size
        val added = super.addAll(elements)
        if (added) {
            notifyAdd(oldSize, size - oldSize)
        }
        return added
    }

    override fun addAll(index: Int, elements: Collection<T>): Boolean {
        val added = super.addAll(index, elements)
        if (added) {
            notifyAdd(index, elements.size)
        }
        return added
    }

    override fun clear() {
        val oldSize = size
        super.clear()
        if (oldSize != 0) {
            notifyRemove(0, oldSize)
        }
    }

    override fun removeAt(index: Int): T {
        val `val` = super.removeAt(index)
        notifyRemove(index, 1)
        return `val`
    }

    override fun set(index: Int, element: T): T {
        val `val` = super.set(index, element)
        if (listeners != null) {
            listeners!!.notifyChanged(this, index, 1)
        }
        return `val`
    }

    public override fun removeRange(fromIndex: Int, toIndex: Int) {
        super.removeRange(fromIndex, toIndex)
        notifyRemove(fromIndex, toIndex - fromIndex)
    }

    private fun notifyAdd(start: Int, count: Int) {
        if (listeners != null) {
            listeners!!.notifyInserted(this, start, count)
        }
    }

    private fun notifyRemove(start: Int, count: Int) {
        if (listeners != null) {
            listeners!!.notifyRemoved(this, start, count)
        }
    }

    fun replaceAll(newList: List<T>, calcDiff: Boolean = true) {
        if (calcDiff) {
            if (updateCallbacks == null) {
                updateCallbacks = object : ListUpdateCallback {
                    override fun onChanged(position: Int, count: Int, payload: Any?) {
                        listeners?.notifyChanged(this@ObservableArrayList, position, count)
                    }

                    override fun onMoved(fromPosition: Int, toPosition: Int) {
                        listeners?.notifyMoved(this@ObservableArrayList, fromPosition, toPosition, 1)
                    }

                    override fun onInserted(position: Int, count: Int) {
                        listeners?.notifyInserted(this@ObservableArrayList, position, count)
                    }

                    override fun onRemoved(position: Int, count: Int) {
                        listeners?.notifyRemoved(this@ObservableArrayList, position, count)
                    }
                }
            }

            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                    return get(oldItemPosition) == newList.get(newItemPosition)
                }

                override fun getOldListSize(): Int = this@ObservableArrayList.size

                override fun getNewListSize(): Int {
                    return newList.size
                }

                override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                    return areItemsTheSame(oldItemPosition, newItemPosition)
                }
            })

            super.clear()
            super.addAll(newList)

            result.dispatchUpdatesTo(updateCallbacks)
        }
        else {
            super.clear()
            super.addAll(newList)
            listeners?.notifyChanged(this)
        }
    }
}
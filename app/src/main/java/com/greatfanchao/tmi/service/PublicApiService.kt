package com.greatfanchao.tmi.service

import com.greatfanchao.tmi.model.Category
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface PublicApiService {

    @GET("/v1/Categories.json?depth=1")
    fun retrieveRootCategory(): Single<Category>

    @GET("/v1/Categories/{catId}.json?depth=1")
    fun retrieveCategory(@Path("catId") categoryId: String): Single<Category>
}
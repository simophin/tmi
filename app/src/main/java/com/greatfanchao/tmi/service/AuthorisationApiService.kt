package com.greatfanchao.tmi.service

import com.greatfanchao.tmi.model.ListingDetails
import com.greatfanchao.tmi.model.ListingSummary
import com.greatfanchao.tmi.service.dto.PaginatedDto
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface AuthorisationApiService {

    @GET("/v1/Search/General.json?photo_size=List&rows=20")
    fun retrieveListingsByCategory(@Query("category") categoryId: String): Single<PaginatedDto<ListingSummary>>

    @GET("/v1/Listings/{listingId}.json")
    fun retrieveListingDetails(@Path("listingId") listingId: String): Single<ListingDetails>
}
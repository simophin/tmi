package com.greatfanchao.tmi.service

data class NoSuchCategoryException(val categoryId: String?) : RuntimeException()
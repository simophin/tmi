package com.greatfanchao.tmi.service.dto

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

data class PaginatedDto<out T> @JsonCreator constructor(
    @JsonProperty("PageSize") val numPages: Int,
    @JsonProperty("Page") val currentPage: Int,
    @JsonProperty("List") val data: List<T>?
)